#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <time.h>

#define UP 0
#define DOWN 1
#define RIGHT 2
#define LEFT 3

int WIDTH, HEIGHT, TILE_SIZE;

void moveSnakeParts(int snake[WIDTH*HEIGHT][2], int snakeSize);
int notInSnake(int snake[WIDTH*HEIGHT][2], int snakeSize, int fruit[2]);
int biteSnake(int snake[WIDTH*HEIGHT][2], int snakeSize, int position[2]);

int main(int argc, char **argv) {
	if(argc == 1) {
		WIDTH = 20;
		HEIGHT = 20;
		TILE_SIZE = 20;
	}
	else if(argc == 3) {
		WIDTH = atoi(argv[1]);
		HEIGHT = atoi(argv[2]);
		TILE_SIZE = 20;
	}
	else if(argc == 4) {
		WIDTH = atoi(argv[1]);
		HEIGHT = atoi(argv[2]);
		TILE_SIZE = atoi(argv[3]);
	}

	// init SDL2
	if(SDL_Init(SDL_INIT_VIDEO) <0) {
		printf("Couldn't init SDL : %s\n", SDL_GetError());
		exit(EXIT_FAILURE);
	}
	// init TTF
	if(TTF_Init()==-1) {
		printf("could not init TTF : %s\n", TTF_GetError());
		exit(EXIT_FAILURE);
	}

	// create a window and renderer
	SDL_Window *window =  SDL_CreateWindow("snake", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WIDTH*TILE_SIZE, HEIGHT*TILE_SIZE, SDL_WINDOW_BORDERLESS | SDL_WINDOW_ALLOW_HIGHDPI);
	SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 0);

	SDL_SetRenderDrawColor( renderer, 0, 0, 255, 255); 
	SDL_RenderFillRect( renderer, NULL); 

	//check renderer was created, else exit
	if(renderer == NULL) {
		printf("Error creating the renderer, exiting now.\n");
		exit(EXIT_FAILURE);
	}
	
	// create two textures and load a picture on it (lazy mode : a color)
	// one is the snake body, one is an empty place on the map
	SDL_Texture *snake_text;
	SDL_Texture *empty_text;
	SDL_Texture *fruit_text;

	SDL_Surface *temp = SDL_CreateRGBSurface(0, TILE_SIZE, TILE_SIZE, 32, 0, 0, 0, 0);
	if(temp == NULL) {
		printf("Error creating the temp. surface, existing now.\n");
		exit(EXIT_FAILURE);
	}
	
	SDL_FillRect(temp, NULL, SDL_MapRGB(temp->format, 240, 230, 240));
	empty_text = SDL_CreateTextureFromSurface(renderer, temp);
	SDL_FillRect(temp, NULL, SDL_MapRGB(temp->format, 240, 0, 0));
	snake_text = SDL_CreateTextureFromSurface(renderer, temp);
	SDL_FillRect(temp, NULL, SDL_MapRGB(temp->format, 0,0,255));
	fruit_text = SDL_CreateTextureFromSurface(renderer, temp);
	SDL_FreeSurface(temp);

	//graphical stuff is loaded. 
	// Now create the variables that will be used on the project	
	
	int i, j;
	int snake[WIDTH * HEIGHT][2];
	int snakeSize = 3;
	snake[0][0] = 2;snake[0][1]=0;
	snake[1][0] = 1;snake[1][1]=0;
	snake[2][0] = 0;snake[2][1]=0;

	SDL_Rect position;
	position.w = TILE_SIZE;
	position.h = TILE_SIZE;
	position.x=0;
	position.y=0;

	// fill the screen with empty tiles
	for(i=0 ; i<WIDTH ; i++) {
		position.x = i*TILE_SIZE;
		for(j=0 ; j<HEIGHT ; j++) {
			position.y = j*TILE_SIZE;
			SDL_RenderCopy(renderer, empty_text, NULL, &position);
		}
	}

	// put the snake on the field
	for(i=0 ; i< snakeSize ; i++) {
		position.x = snake[i][0]*TILE_SIZE;
		position.y = snake[i][1]*TILE_SIZE;

		SDL_RenderCopy(renderer, snake_text, NULL, &position);
	}

	srand(time(NULL));
	SDL_Event event;
	int quit=0, direction = RIGHT;
	int fruit[2];
	int nbTurnFruit[128];
	int nbFruits=0;

	fruit[0] = rand() % WIDTH;
	fruit[1] = rand() % HEIGHT;

	position.x = fruit[0] * TILE_SIZE;
	position.y = fruit[1] * TILE_SIZE;

	SDL_RenderCopy(renderer, fruit_text, NULL, &position);
	SDL_RenderPresent(renderer);
	
	do {
		SDL_PollEvent(&event);
		if(event.type == SDL_QUIT) {
			quit=1;
		}
		else if(event.type == SDL_KEYDOWN) {
			switch(event.key.keysym.scancode) {
				case SDL_SCANCODE_ESCAPE :
					quit=1;
					break;
				case SDL_SCANCODE_LEFT :
					if(direction != RIGHT)
						direction = LEFT;
					break;
				case SDL_SCANCODE_RIGHT :
					if(direction != LEFT) 
						direction = RIGHT;
					break;
				case SDL_SCANCODE_UP :
					if(direction != DOWN) 
						direction = UP;
					break;
				case SDL_SCANCODE_DOWN :
					if(direction != UP)
						direction = DOWN;
					break;
				default :
					break;
			}
		}
		
		// first we clear the tail
		position.x= snake[snakeSize-1][0] * TILE_SIZE;
		position.y= snake[snakeSize-1][1] * TILE_SIZE;
		SDL_RenderCopy(renderer, empty_text, NULL, &position);
		
		// add fruit to the list if needed
		for(i=0 ; i<nbFruits ; i++) {
			nbTurnFruit[i]--;
			if(nbTurnFruit[i] == 0) {
				snake[snakeSize][0] = 0;
				snake[snakeSize][1] = 0;
				snakeSize++;
				SDL_RenderCopy(renderer, snake_text, NULL, &position);
			}
		}

		switch(direction) {
			case RIGHT :
				moveSnakeParts(snake, snakeSize);
				snake[0][0]++;
				break;
			case LEFT :
				moveSnakeParts(snake, snakeSize);
				snake[0][0]--;
				break;
			case UP :
				moveSnakeParts(snake, snakeSize);
				snake[0][1]--;
				break;
			case DOWN :
				moveSnakeParts(snake, snakeSize);
				snake[0][1]++;
				break;
			default :
				break;
		}
		// test that snake isn't out of the field
		if(snake[0][0] < 0) {
			snake[0][0] = WIDTH - 1;
		}
		else if(snake[0][0] == WIDTH) {
			snake[0][0] = 0;
		}
		else if(snake[0][1] < 0) {
			snake[0][1] = HEIGHT - 1;
		}
		else if(snake[0][1] == HEIGHT) {
			snake[0][1] = 0;
		} 

		// print the head
		position.x= snake[0][0] * TILE_SIZE;
		position.y= snake[0][1] * TILE_SIZE;
		SDL_RenderCopy(renderer, snake_text, NULL, &position);

		// if head is on the fruit
		if(snake[0][0] == fruit[0] && snake[0][1] == fruit[1]) {
			nbTurnFruit[nbFruits] = snakeSize;
			nbFruits++;
			do {
				fruit[0] = rand() % WIDTH;
				fruit[1] = rand() % HEIGHT;
			}while (notInSnake(snake, snakeSize, fruit));

			position.x = fruit[0] * TILE_SIZE;
			position.y = fruit[1] * TILE_SIZE;

			SDL_RenderCopy(renderer, fruit_text, NULL, &position);

		}

		if(biteSnake(snake, snakeSize, snake[0])) {
			quit = 1;
		}

		SDL_RenderPresent(renderer);
		SDL_Delay(60);
	}while(!quit);
	
	SDL_Texture *text_score;
	SDL_Rect score_position;
	TTF_Font *font;
	char str_score[256];

	// load the font
	SDL_Color color= {100,0,0};
	font = TTF_OpenFont("/usr/share/fonts/TTF/DejaVuSans.ttf", 32);
	if(!font) {
		printf("Impossible to open font : %s\n", TTF_GetError());;
		exit(EXIT_FAILURE);
	}

	// write some text with the font on a temp surface
	//temp = SDL_CreateRGBSurface(0, 50, 30, 32, 0, 0, 0, 0);
	//SDL_FillRect(temp, NULL, SDL_MapRGB(temp->format, 250, 0, 0));

	sprintf(str_score, "you did %d", snakeSize-3);
	temp = TTF_RenderText_Blended(font, str_score,color);
	if(temp == NULL) {
		printf("Impossible to render the score on surface : %s\n", TTF_GetError());;
		exit(EXIT_FAILURE);
	}

	//create the texture from this surface, set its position and print it
	text_score = SDL_CreateTextureFromSurface(renderer, temp);
	
	score_position.x = (WIDTH*TILE_SIZE)/2 - temp->w/2;
	score_position.y = (HEIGHT*TILE_SIZE)/2 - temp->h/2;
	score_position.w = temp->w;
	score_position.h = temp->h;

	SDL_RenderClear(renderer);
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); 
	SDL_RenderFillRect(renderer, NULL); 
	SDL_RenderCopy(renderer, text_score, NULL, &score_position);

	SDL_RenderPresent(renderer);
	
	quit=0;
	do {
		SDL_WaitEvent(&event);
		if(event.type == SDL_QUIT) {
			quit=1;
		}
		else if(event.type == SDL_KEYDOWN) {
			switch(event.key.keysym.scancode) {
				case SDL_SCANCODE_ESCAPE :
					quit=1;
					break;
				case SDL_SCANCODE_RETURN : 
					quit=1;
					break;
				default :
					break;
			}
		}
	}while(!quit);

	SDL_FreeSurface(temp);
	SDL_DestroyTexture(text_score);
	TTF_CloseFont(font);
	// free the memory before exiting the game. 

	SDL_DestroyTexture(empty_text);
	SDL_DestroyTexture(snake_text);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}
void moveSnakeParts(int snake[WIDTH*HEIGHT][2], int snakeSize) {
	int i;
	for(i=snakeSize-1 ; i>0 ; i--) {
		snake[i][0] = snake[i-1][0];
		snake[i][1] = snake[i-1][1];
	}

}
int notInSnake(int snake[WIDTH*HEIGHT][2], int snakeSize, int fruit[2]) {
	int i;
	for(i=0 ; i<snakeSize ; i++) {
		if(snake[i][0] == fruit[0] && snake[i][1] == fruit[1]) {
			return 1;
		}
	}
	return 0;
}
int biteSnake(int snake[WIDTH*HEIGHT][2], int snakeSize, int position[2]) {
	int i;
	for(i=1 ; i<snakeSize ; i++) {
		if(snake[i][0] == position[0] && snake[i][1] == position[1]) {
			return 1;
		}
	}
	return 0;
}
