# csnake

This is a simple snake game I did in C when I was younger. 

## How to build

You need SDL2 libraries installed to build the game (SDL2 and SDL2_ttf). 

```
gcc -lSDL2 -lSDL2_ttf main.c -o snake
```



Feel free to adapt the code at your will
